# Quasar App (suazo)

_Proyecto realizado como parte de prueba de selección._

## Comenzando 🚀

Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.

### Pre-requisitos 📋

* [Git (opcional)](https://git-scm.com/)
* [Nodejs](https://nodejs.org/en/)
* [Quasar CLI](https://quasar.dev/quasar-cli/installation)

Descargar o clonar el proyecto por la via de su preferencia.

### Instalación 🔧

Teniendo la copia del repositorio en local, es momento de descargar las dependencias para hacerlo funcionar.

En la terminal/cmd moverse a la ubicancion del proyecto y ejecutar.

```bash
npm install
```

## Despliegue 📦

### Compila y recarga en caliente para desarrollo

```bash
quasar dev
```

### Compila y minimiza para produccion

```bash
quasar build
```

## Construido con 🛠️

* [Quasar](https://quasar.dev/)

