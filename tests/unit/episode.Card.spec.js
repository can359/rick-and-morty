import { shallowMount } from '@vue/test-utils'
import EpisodeCard from '@/components/EpisodeCard.vue'

beforeAll(() => {
  jest.spyOn(console, 'log').mockImplementation(() => {});
  jest.spyOn(console, 'error').mockImplementation(() => {});
  jest.spyOn(console, 'warn').mockImplementation(() => {});
  jest.spyOn(console, 'info').mockImplementation(() => {});
  jest.spyOn(console, 'debug').mockImplementation(() => {});
});

describe('EpisodeCard.vue', () => {
  it('props.episode es requerido', () => {
    expect(EpisodeCard.props.episode.required).toBe(true)
  })

  it('mostrar props.episode cuando es pasado', () => {
    const episode = {
      "id": 29,
      "name": "Morty's Mind Blowers",
      "air_date": "September 17, 2017",
      "episode": "S03E08",
      "characters": [
        "https://rickandmortyapi.com/api/character/1",
        "https://rickandmortyapi.com/api/character/2",
        "https://rickandmortyapi.com/api/character/3",
        "https://rickandmortyapi.com/api/character/4",
        "https://rickandmortyapi.com/api/character/5",
        "https://rickandmortyapi.com/api/character/33",
        "https://rickandmortyapi.com/api/character/67",
        "https://rickandmortyapi.com/api/character/147",
        "https://rickandmortyapi.com/api/character/149",
        "https://rickandmortyapi.com/api/character/180",
        "https://rickandmortyapi.com/api/character/242",
        "https://rickandmortyapi.com/api/character/244",
        "https://rickandmortyapi.com/api/character/251",
        "https://rickandmortyapi.com/api/character/272",
        "https://rickandmortyapi.com/api/character/329",
        "https://rickandmortyapi.com/api/character/368",
        "https://rickandmortyapi.com/api/character/377",
        "https://rickandmortyapi.com/api/character/390",
        "https://rickandmortyapi.com/api/character/490",
        "https://rickandmortyapi.com/api/character/491"
      ],
      "url": "https://rickandmortyapi.com/api/episode/29",
      "created": "2017-11-10T12:56:36.726Z"
    }
    const wrapper = shallowMount(EpisodeCard, {
      propsData: {
        episode
      }
    })
    expect(wrapper.text()).toMatch(episode.name)
  })
})
