export function setCharacters (state, payload) {
  state.allCharacters = payload
}

export function setOriginOptions (state, payload) {
  state.originOptions = payload
}
