import Vue from 'vue'
import axios from 'axios'

axios.defaults.baseURL = 'https://rickandmortyapi.com/api'

Vue.prototype.$axios = axios
